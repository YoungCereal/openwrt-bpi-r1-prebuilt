[Mirror Download] - (https://drive.google.com/open?id=0BzpPhtR2CRiIfkM0YWktVUczWVZuWS1sb1M0aVBjR3NudzdHZlFvdlcxLUpYM1ZTRWkxaGc)

## Stable 1.2.2 release:

`* Kernel upgraded to 4.1.20`

`* Added Adblock - filter unwanted Ads to local machines (PC, mobiles etc)`

`* Added uHTTPd luci gui and Clamav, Squid proxy as default`

`* Firewall rules in luci can be date and time enabled or disabled`

`* Resize of Root done via the 'resizeroot.sh' script - run from ssh terminal, then auto reboots to resize on next boot`

`* Added extra options for DNSMASQ in the luci gui`

`* Wifi - Add ability to rename interface in luci, added rtl8192cu patch for stability`

`* Default banner - Shows procedure on firmware upgrade: no need for user to keep taking SD card in/out from board`

`* Lots of package upgrades, security patches and bug fixes (typos,netdev led patch added)`


## Stable 1.2.1 release:

`* Kernel upgraded to 4.1.17`

`* Added Parted package`

`* Lots of package upgrades and bug fixes...`

## Prebuilt images are located at gitlab or google drive.

## VM build system - OpenwrtBuild-BPI-R1-VM-V1.0.torrent if you wish to spin up your own version from scratch!

## Thanks
# db260179

